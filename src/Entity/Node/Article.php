<?php

declare(strict_types=1);

namespace Drupal\bundle_classes\Entity\Node;

use Drupal\bca\Attribute\Bundle;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\Entity\Node;

#[Bundle(
  entityType: 'node',
  bundle: 'article',
)]
final class Article extends Node {

  public function getLastUpdatedDate(): array {
    $lastUpdated = $this->get('changed')->view([
      'type' => 'timestamp',
      'label' => 'inline',
      'settings' => [
        'date_format' => 'custom',
        'custom_date_format' => 'j F Y - g:ia',
      ]
    ]);
    $lastUpdated['#title'] = new TranslatableMarkup('Last updated');
    return $lastUpdated;
  }

}
